const AWS = require('aws-sdk');
const ses = new AWS.SES();
const ddb = new AWS.DynamoDB.DocumentClient();

exports.handler = async (event) => {

    let username = event.username;
    let existingRecords = await checkIfUsernameExists(username);
    let curatedEvent = {...event, 'reqExt': event.reqExt || !!(event.useCase)}

    let saved = await addFeedbackToDB({
        ...curatedEvent,
        username: (existingRecords.length > 0) ? `${event.username}__${existingRecords.length + 1}` : event.username
    });

    let emailSent = await sendEmail(curatedEvent, saved, existingRecords);

    if (saved || emailSent) {
        return true;
    } else {
        throw new Error("Failed to process feedback");
    }
};

async function checkIfUsernameExists(username) {
    try {
        let data = await ddb.scan({
            TableName: "EvaluationFeedback",
            ScanFilter: {
                'username': {
                    ComparisonOperator: "BEGINS_WITH",
                    AttributeValueList: [username]
                }
            }
        }).promise();

        return data.Items || [];

    } catch (err) {
        console.log("Failed to check if username", username, "already exists", err);
        return [];
    };
}

async function addFeedbackToDB(feedback) {
    try {
        await ddb.put({
            TableName: "EvaluationFeedback",
            Item: feedback
        }).promise();
        console.log("Successfully saved feedback to DDB");
        return true;

    } catch (err) {
        console.log("Failed to save feedback to DDB", err);
        return false;
    };
}

async function sendEmail(feedback, savedToDB, existingRecords) {
    let email = generateFeedbackEmail(feedback, savedToDB, existingRecords);
    try {
        let data = await ses.sendEmail({
            Source: "noreply@slappforge.com",
            Destination: {
                ToAddresses: [process.env.TO_EMAIL]
            },
            Message: {
                Subject: {
                    Data: "Sigma Evaluation Feedback"
                },
                Body: {
                    Html: {
                        Data: email
                    }
                }
            }
        }).promise();
        console.log("Successfully sent feedback notification email");
        return true;

    } catch (err) {
        console.log("Failed to send feedback notification email", err);
        return false;
    };
}

function generateFeedbackEmail(feedback, savedToDB, existingRecords) {
    let email = `<table>
                            <tr>
                                <td>Username</td>
                                <td>${feedback.username}</td>
                            </tr>
                            <tr>
                                <td>Name</td>
                                <td>${feedback.name}</td>
                            </tr>
                            <tr>
                                <td>Email</td>
                                <td>${feedback.email}</td>
                            </tr>
                            <tr>
                                <td>Environment</td>
                                <td>${feedback.environment}</td>
                            </tr>
                            <tr>
                                <td>Rating</td>
                                <td>${feedback.rating}</td>
                            </tr>
                            <tr>
                                <td>Comment</td>
                                <td>${feedback.feedback || '-'}</td>
                            </tr>
                            <tr>
                                <td>Extension Requested</td>
                                <td>${feedback.reqExt ? 'Yes' : 'No'}</td>
                            </tr>
                            <tr>
                                <td>Use Case</td>
                                <td>${feedback.useCase || '-'}</td>
                            </tr>
                            <tr>
                                <td>Added to DDB</td>
                                <td>${savedToDB}</td>
                            </tr>
                        </table>`;

    if (existingRecords.length > 0) {
        email += `<br/><div>${existingRecords.length} more feedback/s found from the same username</div>`
    }
    return email;
}